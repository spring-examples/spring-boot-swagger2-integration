/**
 * 
 */
package com.sarya.spring.swagger.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Sarang Lohar
 *
 */

@RestController
public class MainController {

	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "/api/hello")
	@ApiOperation(value = "Get the api hello api")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Success By Sarang", response = String.class),
			@ApiResponse(code = 401, message = "Unauthorized By Sarang"),
			@ApiResponse(code = 403, message = "Forbidden By Sarang"),
			@ApiResponse(code = 404, message = "Not Found By Sarang"),
			@ApiResponse(code = 500, message = "Failure By Sarang") })
	public String sayHello() {
		return "Swagger Hello World";
	}

	@ResponseBody
	@RequestMapping(method = RequestMethod.GET, value = "/api/hello1")
	@ApiOperation(value = "Get the api hello1 api")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Success By Sarang", response = String.class),
			@ApiResponse(code = 401, message = "Unauthorized By Sarang"),
			@ApiResponse(code = 403, message = "Forbidden By Sarang"),
			@ApiResponse(code = 404, message = "Not Found By Sarang"),
			@ApiResponse(code = 500, message = "Failure By Sarang") })
	public String sayHello1() {
		return "Swagger Hello World Sarang";
	}
}