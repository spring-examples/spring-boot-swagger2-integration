/**
 * 
 */
package com.sarya.spring.swagger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Sarang Lohar
 *
 */
@SpringBootApplication
public class SpringBootSwaggerIntegration {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSwaggerIntegration.class, args);
	}

}
